package com.example.restservice.repository;

import com.example.restservice.model.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.persistence.LockModeType;
import java.util.List;

@Repository
public interface AccountRepository extends JpaRepository<Account, Long> {
    @Query(value = "SELECT * FROM accounts ORDER BY account_id", nativeQuery = true)
    List<Account> findAll();

    @Lock(LockModeType.PESSIMISTIC_WRITE)
    @Query("from Account a where a.id in :ids")
    List<Account> findAllByAccountIds(@Param("ids") Iterable<Long> ids);
}
