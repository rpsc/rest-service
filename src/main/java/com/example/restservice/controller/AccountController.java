package com.example.restservice.controller;

import com.example.restservice.form.SendMoneyForm;
import com.example.restservice.model.Account;
import com.example.restservice.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
public class AccountController {

    @Autowired
    private AccountService accountService;

    @RequestMapping(value = "/accounts", method = RequestMethod.GET)
    @ResponseBody
    public List<Account> getAllAccounts() {
        return accountService.getAllAccounts();
    }

    @PostMapping("/accounts")
    public Account createAccount(@Valid @RequestBody Account account) {
        return accountService.createAccount(account);
    }

    @PostMapping("/remittance")
    @ResponseStatus(HttpStatus.OK)
    public void processSendMoney(@Valid @RequestBody SendMoneyForm sendMoneyForm) {
        accountService.transfer(sendMoneyForm);
    }
}