package com.example.restservice.form;

import javax.validation.constraints.NotNull;

public class SendMoneyForm {

    @NotNull(message = "accSrc not be null")
    private Long accSrc;

    @NotNull(message = "accDst not be null")
    private Long accDst;

    @NotNull(message = "amount not be null")
    private Double amount;

    public Long getAccSrc() {
        return accSrc;
    }

    public void setAccSrc(Long accSrc) {
        this.accSrc = accSrc;
    }

    public Long getAccDst() {
        return accDst;
    }

    public void setAccDst(Long accDst) {
        this.accDst = accDst;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }
}