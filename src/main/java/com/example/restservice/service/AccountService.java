package com.example.restservice.service;

import com.example.restservice.exceptions.NotEnoughMoneyException;
import com.example.restservice.exceptions.ResourceNotFoundException;
import com.example.restservice.form.SendMoneyForm;
import com.example.restservice.model.Account;
import com.example.restservice.repository.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;

@Service
public class AccountService {

    @Autowired
    private AccountRepository accountRepository;

    @Transactional(
            rollbackFor = {NotEnoughMoneyException.class, ResourceNotFoundException.class}
    )
    public void transfer(SendMoneyForm sendMoneyForm) {
        List<Account> foundedAccounts;
        List<Long> accountIds = new LinkedList<>();
        accountIds.add(sendMoneyForm.getAccSrc());
        accountIds.add(sendMoneyForm.getAccDst());
        foundedAccounts = accountRepository.findAllByAccountIds(accountIds);
        if (foundedAccounts.size() < 2) {
            throw new ResourceNotFoundException("resource to transfer remittance not found");
        }
        foundedAccounts.forEach(account -> {
                    if (!account.equals(foundedAccounts.get(foundedAccounts.size() - 1))) {
                        if (account.getValue() >= sendMoneyForm.getAmount()) {
                            account.setValue(account.getValue() - sendMoneyForm.getAmount());
                        } else {
                            throw new NotEnoughMoneyException("insufficient cash account:" + account.getAccountNumber());
                        }
                    } else {
                        account.setValue(account.getValue() + sendMoneyForm.getAmount());
                    }
                }
        );
    }

    public List<Account> getAllAccounts() {
        return accountRepository.findAll();
    }

    public Account createAccount(Account account) {
        return accountRepository.save(account);
    }
}


