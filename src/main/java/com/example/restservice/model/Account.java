package com.example.restservice.model;

import javax.persistence.*;

@Entity
@Table(name = "accounts")
public class Account extends AuditModel {
    @Id
    @GeneratedValue
    @Column(name = "account_id")
    private Long accountId;

    @Column(name = "account_number", unique = true, nullable = false)
    private Long accountNumber;

    @Column(name = "value", nullable = false)
    private Double value;

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    public Long getAccountNumber() {
        return accountNumber;
    }
}